from math import pi


class Shape(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def get_square(self):
        raise NotImplementedError()

    def get_center(self):
        return self.x, self.y


class Point(Shape):
    def __init__(self, x, y):
        super().__init__(x, y)

    def get_square(self):
        return "Your can't calculate square from Point"


class Circle(Shape):
    def __init__(self, x, y, r):
        super().__init__(x, y)
        self.r = r

    def get_square(self):
        return pi * (self.r**2)

    def __contains__(self, x_1, x_2):
        if x_1 and x_2 <= self.r:
            return True
        else:
            return False


p = Point(1, 52)
c = Circle(0, 0, 52)
print(c.__contains__(p.x, p.y))
