from math import sqrt, pi


class Shape(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def get_square(self):
        raise NotImplementedError()

    def get_center(self):
        return self.x, self.y


class EquilateralTriangle(Shape):
    def __init__(self, x, y, z):
        super().__init__(x, y)
        self.z = z

    def get_square(self):
        return self.z*sqrt(3)/2


class Rectangle(Shape):
    def __init__(self, x, y, a, b):
        super().__init__(x, y)
        self.a = a
        self.b = b

    def get_square(self):
        return self.a*self.b


class Circle(Shape):
    def __init__(self, x, y, r):
        super().__init__(x, y)
        self.r = r

    def get_square(self):
        return pi * (self.r**2)


class Square(Shape):
    def __init__(self, x, y, a):
        super().__init__(x, y)
        self.a = a

    def get_square(self):
        return self.a**2


class RoundRectangle(Shape):  # Area = ab - r² * ( 4 - π )
    def __init__(self, x, y, a, b, r_of_corner):
        super().__init__(x, y)
        self.a = a
        self.b = b
        self.r_of_corner = r_of_corner

    def get_square(self):
        return self.a * self.b - self.r_of_corner**2 * (4 - pi)


class ShapeContainer:
    def __init__(self):
        self.list_of_shapes = []

    def append(self, shape):
        self.list_of_shapes.append(shape)

    def pop(self):
        self.list_of_shapes.pop()

    def size(self):
        return len(self.list_of_shapes)

    def get_square(self):
        sum_of_squares = 0
        for item in self.list_of_shapes:
            sum_of_squares += item.get_square()
        return sum_of_squares


e1 = EquilateralTriangle(1, 1, 10)
r1 = Rectangle(1, 1, 10, 7)
r2 = Rectangle(1, 1, 23, 9)
c1 = Circle(1, 1, 12)
c2 = Circle(1, 1, 43)
s1 = Square(1, 1, 222)
rr1 = RoundRectangle(1, 1, 10, 23, 5)


container = ShapeContainer()
container.append(e1)
container.append(r1)
container.append(r2)
print(container.get_square())
print(container.size())
container.pop()
print(container.get_square())
print(container.size())
print(rr1.get_square())


